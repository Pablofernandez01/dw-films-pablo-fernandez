<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gestión de Películas</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">


    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-light navbar-fixed-top navbar-expand-md bg-faded" role="navigation" style="background-color: #e3f2fd;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="../../index.php"> <img src="../../assets/img/small-logo.png" alt="" ></a>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                  <ul class="navbar-nav mr-auto ">
                    <li class="nav-item active">
                      <a type="button" class="btn btn-info " href="#">Añadir una película</a>
                    </li>
                  </ul>
                    
                </div>
              </nav>
        <!-- Page Content -->
         <div class="container">
            <form class="form-horizontal" method="post" action="../controllers/insertController.php">
                
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="title" placeholder="Name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="synopsis" class="col-sm-2 control-label">Synopsis</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="synopsis" name="synopsis" placeholder="Synopsis" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="director" class="col-sm-2 control-label">Director</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="director" name="director" placeholder="Director" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cast" class="col-sm-2 control-label">Reparto</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="cast" name="cast" placeholder="Cast" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="year" class="col-sm-2 control-label">Año</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="year" name="year" placeholder="Year" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="recommendedAge" class="col-sm-2 control-label">Edad Recomendada</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="recommendedAge" name="recommendedAge" placeholder="Recommended Age" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="duration" class="col-sm-2 control-label">Duración (h/m/s)</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="duration" name="duration" placeholder="Duration" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mark" class="col-sm-2 control-label">Puntuación (Sobre 10)</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="mark" name="mark" placeholder="Mark" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="poster" class="col-sm-2 control-label">Poster</label>
                    <div class="col-sm-10">
                        <input type="url" class="form-control" id="poster" name="poster" placeholder="Poster" value="">
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Añadir</button>
                    </div>
                </div>
            </form>

            <!-- Footer -->
           <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <br>
                        <p>Copyright &copy; P.F 2021</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../../assets/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../assets/js/bootstrap.min.js"></script>
    </body>

</html>


