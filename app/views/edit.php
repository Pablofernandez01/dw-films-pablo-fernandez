<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/FilmDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Film.php');


//Compruebo que me llega por GET el parámetro
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    $filmDAO = new FilmDAO();
    $film = $filmDAO->selectById($id);
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gestión de películas</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">


    </head>
    <body>
         <!-- Navigation -->
        <nav class="navbar navbar-light navbar-fixed-top navbar-expand-md bg-faded" role="navigation" style="background-color: #e3f2fd;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="../../index.php"> <img src="../../assets/img/small-logo.png" alt="" ></a>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                  <ul class="navbar-nav mr-auto ">
                    <li class="nav-item active">
                      <a type="button" class="btn btn-info " href="app/views/insert.php">Añadir una película</a>
                    </li>
                  </ul>
                    
                </div>
              </nav>

        <!-- Page Content -->
        <div class="container">
            <form class="form-horizontal" method="post" action="../controllers/editController.php">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $film->getName(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="synopsis" class="col-sm-2 control-label">Synopsis</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="synopsis" name="synopsis" placeholder="Synopsis" value="<?php echo $film->getSynopsis(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="director" class="col-sm-2 control-label">Director</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="director" name="director" placeholder="Director" value="<?php echo $film->getDirector(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cast" class="col-sm-2 control-label">Reparto</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="cast" name="cast" placeholder="Cast" value="<?php echo $film->getCast(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="year" class="col-sm-2 control-label">Año</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="year" name="year" placeholder="Year" value="<?php echo $film->getYear(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="recommendedAge" class="col-sm-2 control-label">Edad Recomendada</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="recommendedAge" name="recommendedAge" placeholder="Recommended Age" value="<?php echo $film->getRecommendedAge(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="duration" class="col-sm-2 control-label">Duración (h/m/s)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration" value="<?php echo $film->getDuration(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mark" class="col-sm-2 control-label">Puntuación (Sobre 10)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="mark" name="mark" placeholder="Mark" value="<?php echo $film->getMark(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="poster" class="col-sm-2 control-label">Poster</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="poster" name="poster" placeholder="Poster" value="<?php echo $film->getPoster(); ?>">
                    </div>
                </div>
                <input type="hidden" name="id" value="<?php echo $film->getIdFilm(); ?>">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Editar</button>
                    </div>
                </div>
            </form>

            <!-- Footer -->
           <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <br>
                        <p>Copyright &copy; P.F 2021</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../../assets/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../assets/js/bootstrap.min.js"></script>
    </body>

</html>


