<?php

class Film {

    private $idFilm;
    private $name;
    private $synopsis;
    private $poster;
    private $director;
    private $cast;
    private $year;
    private $recommendedAge;
    private $duration;
    private $mark;

    public function __construct() {
        
    }
    
    public function getIdFilm() {
        return $this->idFilm;
    }

    public function getName() {
        return $this->name;
    }

    public function getSynopsis() {
        return $this->synopsis;
    }

    public  function getPoster() {
        return $this->poster;
    }
    
    public  function getDirector() {
        return $this->director;
    }
    
    public  function getCast() {
        return $this->cast;
    }
    
    public  function getYear() {
        return $this->year;
    }
    
    public  function getRecommendedAge() {
        return $this->recommendedAge;
    }
    
    public  function getDuration() {
        return $this->duration;
    }
    
    public  function getMark() {
        return $this->mark;
    }

    public function setIdFilm($idFilm) {
        $this->idFilm = $idFilm;
    }

    public function setName($name) {
        $this->name = $name;
    }

    function setSynopsis($synopsis) {
        $this->synopsis = $synopsis;
    }
    
    public function setPoster($poster) {
        $this->poster = $poster;
    }
    
    public function setDirector($director) {
        $this->director = $director;
    }
    
    public function setCast($cast) {
        $this->cast = $cast;
    }
    
    public function setYear($year) {
        $this->year = $year;
    }
    
    public function setRecommendedAge($recommendedAge) {
        $this->recommendedAge = $recommendedAge;
    }

    public function setDuration($duration) {
        $this->duration = $duration;
    }
    
    public function setMark($mark) {
        $this->mark = $mark;
    }
    
//Función para pintar cada película
    function Film2HTML() {
        $result = '<div class=" col-md-4 ">';
         $result .= '<div class="card ">';
          $result .= ' <img class="card-img-top rounded mx-auto d-block avatar" src='.$this->getPoster().' alt="Card image cap">';
            $result .= '<div class="card-block">';
                $result .= '<h2 class="card-title">' . $this->getName() . '</h2>';
                $result .= '<p class=" card-text description"><strong>Synopsis: </strong>'.$this->getSynopsis().'</p>'; 
                $result .= '<p class=" card-text description"><strong>Director: </strong>'.$this->getDirector().'</p>';                    
                $result .= '<p class=" card-text description"><strong>Reparto: </strong>'.$this->getCast().'</p>';                    
                $result .= '<p class=" card-text description"><strong>Año: </strong>'.$this->getYear().'</p>';                    
                $result .= '<p class=" card-text description"><strong>Edad Recomendada: </strong>'.$this->getRecommendedAge().' años</p>';                    
                $result .= '<p class=" card-text description"><strong>Duración: </strong>'.$this->getDuration().'</p>';                    
                $result .= '<p class=" card-text description"><strong>Puntuación: </strong>'.$this->getMark().'/10</p>';                    
             $result .= '</div>';
             $result .= ' <div  class=" btn-group card-footer" role="group">';
                $result .= '<a type="button" class="btn btn-secondary" href="app/views/detail.php?id='.$this->getIdFilm().'">Detalles</a>';
                $result .= '<a type="button" class="btn btn-success" href="app/views/edit.php?id='.$this->getIdFilm().'">Modificar</a> ';
                $result .= '<a type="button" class="btn btn-danger" href="app/controllers/deleteController.php?id='.$this->getIdFilm().'">Borrar</a> ';
            $result .= ' </div>';
         $result .= '</div>';
     $result .= '</div>';
        
        
        return $result;
    }
    
    
}
