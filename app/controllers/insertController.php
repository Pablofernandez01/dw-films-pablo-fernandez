<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/FilmDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Film.php');
require_once(dirname(__FILE__) . '/../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    $name = ValidationsRules::test_input($_POST["name"]);
    $synopsis = ValidationsRules::test_input($_POST["synopsis"]);
    $poster = ValidationsRules::test_input($_POST["poster"]);
    $director = ValidationsRules::test_input($_POST["director"]);
    $cast = ValidationsRules::test_input($_POST["cast"]);
    $year = ValidationsRules::test_input($_POST["year"]);
    $recommendedAge = ValidationsRules::test_input($_POST["recommendedAge"]);
    $duration = ValidationsRules::test_input($_POST["duration"]);
    $mark = ValidationsRules::test_input($_POST["mark"]);

    // TODOD hacer uso de los valores validados 
    $film = new Film();
    $film->setName($_POST["name"]);
    $film->setSynopsis($_POST["synopsis"]);
    $film->setPoster($_POST["poster"]);
    $film->setDirector($_POST["director"]);
    $film->setCast($_POST["cast"]);
    $film->setYear($_POST["year"]);
    $film->setRecommendedAge($_POST["recommendedAge"]);
    $film->setDuration($_POST["duration"]);
    $film->setMark($_POST["mark"]);

    //Creamos un objeto FilmDAO para hacer las llamadas a la BD
    $filmDAO = new FilmDAO();
    $filmDAO->insert($film);
    
    header('Location: ../../index.php');
    
}
?>

