<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/FilmDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Film.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $name = $_POST["name"];
    $synopsis = $_POST["synopsis"];
    $poster = $_POST["poster"];
    $director = $_POST["director"];
    $cast = $_POST["cast"];
    $year = $_POST["year"];
    $recommendedAge = $_POST["$recommendedAge"];
    $duration = $_POST["duration"];
    $mark = $_POST["mark"];
    
    $film = new Film();
    $film->setIdFilm($id);
    $film->setName($name);
    $film->setSynopsis($synopsis);
    $film->setPoster($poster);
    $film->setDirector($director);
    $film->setCast($cast);
    $film->setYear($year);
    $film->setRecommendedAge($recommendedAge);
    $film->setDuration($duration);
    $film->setMark($mark);

    $filmDAO = new FilmDAO();
    $filmDAO->update($film);

    header('Location: ../../index.php');
}

?>

