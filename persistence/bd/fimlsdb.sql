CREATE DATABASE IF NOT EXISTS filmsdb;

USE filmsdb;

CREATE TABLE IF NOT EXISTS films(
	idFilm int not null AUTO_INCREMENT,
	name varchar(255),
	synopsis text,
	poster varchar(255),
	director varchar(255),
	cast varchar(255),
	year varchar(255),
	recommendedAge varchar(255),
	duration varchar(255),
	mark varchar(255),
	PRIMARY KEY( idFilm )
);

INSERT INTO `films`VALUES (1,'Intrusión','Tras sufrir una invasión de su casa con un trágico desenlace, una mujer conmocionada busca respuestas y descubre que la verdadera pesadilla no ha hecho más que empezar.
.','C:\xampp\htdocs\dw_Films_PabloFernandez\assets\img\intrusion.jpg', 'Adam Salky', 'Cast: 
<ul>
<li>Freida Pinto</li>
<li>Logan Marshall - Green</li>
<li>Robert John Burke</li>
<li>Megan Elisabeth Kelly</li>
</ul>
.','2021','16',1h32min,'5.2');

INSERT INTO `films`VALUES (2,'El practicante','Ángel trabaja como técnico en emergencias sanitarias. Tras sufrir un grave accidente, su vida junto a Vane empieza a desmoronarse. Obsesionado con la idea de que ella le es infiel, su vida se convertirá en un infierno.
.','C:\xampp\htdocs\dw_Films_PabloFernandez\assets\img\el_practicante.jpg', 'Carles Torras', 'Cast: 
<ul>
<li>Mario Casas</li>
<li>Déborah François</li>
<li>Linda Morselli</li>
<li>Guillermo Pfening</li>
</ul>
.','2020','16',1h34min,'5.6');