<?php

//dirname(__FILE__) Es el directorio del archivo actual
require_once(dirname(__FILE__) . '/../conf/PersistentManager.php');

class FilmDAO {

    //Se define una constante con el nombre de la tabla
    const FILM_TABLE = 'films';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . FilmDAO::FILM_TABLE;
        $result = mysqli_query($this->conn, $query);
        $films = array();
        while ($fimlBD = mysqli_fetch_array($result)) {

            $film = new Film();
            $film->setIdFilm($fimlBD["idFilm"]);
            $film->setName($fimlBD["name"]);
            $film->setSynopsis($fimlBD["synopsis"]);
            $film->setPoster($fimlBD["poster"]);
            $film->setDirector($fimlBD["director"]);
            $film->setCast($fimlBD["cast"]);
            $film->setYear($fimlBD["year"]);
            $film->setRecommendedAge($fimlBD["recommendedAge"]);
            $film->setDuration($fimlBD["duration"]);
            $film->setMark($fimlBD["mark"]);

            //array_push($films, $film);
            $films[] = $film;
        }
        return $films;
    }

    public function insert($film) {
        $query = "INSERT INTO " . FilmDAO::FILM_TABLE .
                " (name, synopsis, poster, director, cast, year, recommendedAge, duration, mark) VALUES(?,?,?,?,?,?,?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $film->getName();
        $synopsis = $film->getSynopsis();
        $poster = $film->getPoster();
        $director = $film->getDirector();
        $cast = $film->getCast();
        $year = $film->getYear();
        $recommendedAge = $film->getRecommendedAge();
        $duration = $film->getDuration();
        $mark = $film->getMark();
        
        mysqli_stmt_bind_param($stmt, 'sssssssss', $name, $synopsis, $poster, $director, $cast, $year, $recommendedAge, $duration, $mark);
        return $stmt->execute();
    }

    public function selectById($id) {
        $query = "SELECT name, synopsis, poster, director, cast, year, recommendedAge, duration, mark FROM " . FilmDAO::FILM_TABLE . " WHERE idFilm=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $synopsis, $poster, $director, $cast, $year, $recommendedAge, $duration, $mark);

        $film = new Film();
        while (mysqli_stmt_fetch($stmt)) {
            $film->setIdFilm($id);
            $film->setName($name);
            $film->setSynopsis($synopsis);
            $film->setPoster($poster);
            $film->setDirector($director);
            $film->setCast($cast);
            $film->setYear($year);
            $film->setRecommendedAge($recommendedAge);
            $film->setDuration($duration);
            $film->setMark($mark);
       }

        return $film;
    }

    public function update($film) {
        $query = "UPDATE " . FilmDAO::FILM_TABLE .
                " SET name=?, synopsis=?, poster=?, director=?, cast=?, year=?, recommendedAge=?, duration=?, mark=?"
                . " WHERE idFilm=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $film->getName();
        $synopsis= $film->getSynopsis();
        $poster = $film->getPoster();
        $director = $film->getDirector();
        $cast = $film->getCast();
        $year = $film->getYear();
        $recommendedAge = $film->getRecommendedAge();
        $duration = $film->getDuration();
        $mark = $film->getMark();
        $id = $film->getIdFilm();
        mysqli_stmt_bind_param($stmt, 'sssssssssi', $name, $synopsis, $poster, $director, $cast, $year, $recommendedAge, $duration, $mark, $id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . FilmDAO::FILM_TABLE . " WHERE idFilm=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

        
}

?>
